Summary: X.Org X11 libXmu/libXmuu runtime libraries
Name: libXmu
Version: 1.1.4
Release: 4%{?dist}
License: MIT
URL: http://www.x.org
Source0: https://www.x.org/pub/individual/lib/%{name}-%{version}.tar.xz

BuildRequires: make autoconf automake libtool
BuildRequires: xorg-x11-util-macros libX11-devel libXext-devel libXt-devel
BuildRequires: xmlto

%description
X.Org X11 libXmu/libXmuu runtime libraries


%package devel
Summary: X.Org X11 libXmu development package
Requires: %{name} = %{version}-%{release}

%description devel
X.Org X11 libXmu development package


%prep
%setup -q

%build
autoreconf -fiv
%configure --disable-static
%make_build


%install
%make_install
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
rm -rf $RPM_BUILD_ROOT%{_docdir}


%files
%license COPYING
%doc README.md ChangeLog
%{_libdir}/libXmu.so.6
%{_libdir}/libXmu.so.6.2.0
%{_libdir}/libXmuu.so.1
%{_libdir}/libXmuu.so.1.0.0


%files devel
%dir %{_includedir}/X11/Xmu
%{_includedir}/X11/Xmu/*.h
%{_libdir}/libXmu.so
%{_libdir}/libXmuu.so
%{_libdir}/pkgconfig/xmu.pc
%{_libdir}/pkgconfig/xmuu.pc


%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.1.4-4
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.1.4-3
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.1.4-2
- Rebuilt for OpenCloudOS Stream 23.09

* Wed Aug 2 2023 Shuo Wang <abushwang@tencent.com> - 1.1.4-1
- update to 1.1.4

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.1.3-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.1.3-2
- Rebuilt for OpenCloudOS Stream 23

* Mon Nov 14 2022 rockerzhu <rockerzhu@tencent.com> - 1.1.3-1
- Initail build
